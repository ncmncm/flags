= Flags

Map ordinary enumeration values `0..end` to boolean option flags 1,2,4,8,..
with implicit conversions and logical bitwise operations.

  enumeration::enumerator => Flags<enumeration>(eumeration::enumerator).

The only requirement on the enumeration is a terminating value `end`
or `End`.
