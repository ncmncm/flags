#include <type_traits>
#include <initializer_list>
#include <bitset>
#include <iostream>
#include <array>

template <typename Enum>
  requires std::is_enum<Enum>::value &&
    (requires { Enum::end; } || requires { Enum::End; })
struct Flags {
  constexpr static auto end = [] {
    if constexpr (requires { Enum::end; }) return Enum::end; else return Enum::End; }();
  constexpr static inline auto bitcount = std::size_t(end);

  std::bitset<bitcount> value{};

  constexpr Flags(Enum e) { value.set(unsigned(e)); }
  constexpr Flags(std::initializer_list<Enum> es) {
    for (auto e : es) value.set(unsigned(e));
  }

  constexpr Flags& operator|=(Flags f) { value |= f.value; return *this; }
  constexpr Flags& operator&=(Flags f) { value &= f.value; return *this; }
  constexpr Flags& operator^=(Flags f) { value ^= f.value; return *this; }
  constexpr Flags& operator-=(Flags f) { value &= ~f.value; return *this; }

  friend constexpr Flags operator|(Flags f1, Flags f2) { return f1 |= f2; }
  friend constexpr Flags operator&(Flags f1, Flags f2) { return f1 &= f2; }
  friend constexpr Flags operator^(Flags f1, Flags f2) { return f1 ^= f2; }
  friend constexpr Flags operator-(Flags f1, Flags f2) { return f1 -= f2; }

  friend constexpr Flags operator|(Enum e, Flags f) { return f |= Flags{e}; }
  friend constexpr Flags operator&(Enum e, Flags f) { return f &= Flags{e}; }
  friend constexpr Flags operator^(Enum e, Flags f) { return f ^= Flags{e}; }

  friend constexpr Flags operator|(Flags f, Enum e) { return f |= Flags{e}; }
  friend constexpr Flags operator&(Flags f, Enum e) { return f &= Flags{e}; }
  friend constexpr Flags operator^(Flags f, Enum e) { return f ^= Flags{e}; }
  friend constexpr Flags operator-(Flags f, Enum e) { return f -= Flags{e}; }

  constexpr explicit operator bool() const { return value.any(); }
  constexpr auto operator<=>(Flags f) const { return value <=> f.value; }
};

template <typename Enum>
const std::array<char const*,Flags<Enum>::bitcount> enumNames;

template <typename Enum>
auto operator<<(std::ostream& os, Flags<Enum> f) -> std::ostream& {
  static_assert(Flags<Enum>::bitcount == std::size_t(Flags<Enum>::end));
  auto sep = "";
  for (auto i = 0u; i != Flags<Enum>::bitcount; ++i) {
    if (f & Enum(i)) {
      os << sep << enumNames<Enum>[i];
      sep = " ";
    }
  }
  return os;
}

enum class pizza : unsigned { cheese, sauce, pesto, salami, garlic,  end };
template <> const std::array
enumNames<pizza> = { "cheese", "sauce", "pesto", "salami", "garlic" };

void order(Flags<pizza> tops) {
  std::cout << tops << '\n';
}

enum class Sundae { Syrup, Fudge, Banana, Whip,  End };

template <> const std::array
enumNames<Sundae> = { "Syrup", "Fudge", "Banana", "Whip" };

void order(Flags<Sundae> tops) {
  std::cout << tops << '\n';
}

int main(int, char**) {
  { using enum pizza;
    order(cheese);
    order(Flags{cheese});
    order({cheese});
    order(Flags{cheese} | sauce | garlic);
    order(Flags{cheese, sauce, garlic});
    order({cheese, sauce, garlic});
  }
  { using enum Sundae;
    Flags mix = { Syrup, Banana, Whip };
    order(mix);
    order(mix - Banana);
  }
}
